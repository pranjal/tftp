import socket

conn = socket.create_connection((input("Enter IP: "), 9999))

header = conn.recv(4)
while len(header) != 4:
    header += conn.recv(4 - len(header))

size = (header[0] * 16777216) + (header[1] * 65536) + (header[2] * 256) + header[3]
f = open("file", "wb")
while f.tell() != size:
    f.write(conn.recv(size - f.tell()))

conn.close()
f.close()
