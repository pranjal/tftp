import socket

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
s.bind(('', 9999))
s.listen(1)

file = open(input("Enter a filename: "), "rb")
send = file.read()
header = bytes((len(send)//16777216, (len(send)//65536)%256, (len(send)//256)%256, len(send)%256))

conn = s.accept()[0]
conn.sendall(header + send)
conn.close()
file.close()
